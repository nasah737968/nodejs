let hello_msg = "Hello! I will Learn Local Module(Hello Msg, Func, Array, Obj)";

const getName = () => {
    return "Hasan Mahmud";
}

const getAge = () => {
    return 24;
}

const personalInfo = [
    'Hasan Mahmud',
    24,
    'I like Coding'
];

const info = {};
info['firstname'] = 'Hasan';
info['lastname'] = 'Mahmud';

module.exports = {
    hello_msg,
    getName,
    getAge,
    personalInfo,
    info
}