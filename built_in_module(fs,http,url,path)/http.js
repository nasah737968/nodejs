const http = require('http');
const port = 3000;
const host = '127.0.0.1';

const tempServer = http.createServer((req, res) => {
    res.end("<h1>Hello, Your server created</h1>");
});

tempServer.listen(port, host, () => {
    console.log(`Server is running successfully at http://${host}:${port}`);
});