const http = require('http');
const port = 3000;
const host = '127.0.0.1';

const tempServer = http.createServer((req, res) => {
    res.writeHead(202, { 'Content-Type': 'text/html' });
    res.write("<h1>Hello, Your server created</h1>");
    res.end();
});

tempServer.listen(port, host, () => {
    console.log(`Server is running successfully at http://${host}:${port}`);
});