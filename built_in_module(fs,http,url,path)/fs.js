const fs = require('fs');

console.log("I will learn Built in Async function (writeFile,appendFile,readFile,renameFile,unlink,exist)");

fs.writeFile('./assets/demo1.txt', "This is Demo File", function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log("Success!");
    }
});

fs.appendFile('./assets/demo2.txt', "This is Append Demo File update", function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log("Success!");
    }
});

fs.readFile('./assets/demo2.txt', 'utf-8', function(err, data) {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
});

fs.rename('./assets/rename2.txt', './assets/rename2.txt', (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log('File Rename Success!')
    }
});

fs.unlink('./assets/demo1.txt', (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log('Deleted Success');
    }
});

fs.exists('./assets/demo2.txt', (result) => {
    if (result) {
        console.log('File Exist');
    } else {
        console.log('File not Exist');
    }
});

console.log("I will learn Built in Sync function (readFileSync)");

const syncData = fs.readFileSync('./assets/demo2.txt');
console.log(syncData);